var content = document.getElementsByClassName("content");
[...content].forEach(item=>item.style.display="none");
[...content][0].style.display="block";
function tabFunc(evt, tabName) {
    var i, tabs;
    for (i = 0; i < content.length; i++) {
        content[i].style.display = "none";
    }
    tabs = document.getElementsByClassName("tabs-title");
    for (i = 0; i < tabs.length; i++) {
        tabs[i].className = tabs[i].className.replace("active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}