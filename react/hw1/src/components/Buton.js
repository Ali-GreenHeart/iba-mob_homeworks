import React from "react";
import PropTypes from 'prop-types';

class Button extends React.Component {
    render() {
        return (
              <button className='mainBtn'
                   onClick={this.props.clickHandler}
                   data-designation={this.props.numOrder}
                   style={{backgroundColor: this.props.backgroundColor}}>
                   Open {this.props.numOrder} modal
              </button>
        )
    }
}

Button.propTypes = {
    clickHandler: PropTypes.func.isRequired,
    numOrder: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string.isRequired,
};

export default Button