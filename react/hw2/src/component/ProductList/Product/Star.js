import React from 'react';

const Star = (props) => {
    return (
        <i className="fas fa-star">{props.count}</i>
    );
};

export default Star;