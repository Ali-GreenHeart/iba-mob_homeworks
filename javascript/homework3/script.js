function createNewUser (name,surname) {
    return {
        name: name,
        surname: surname
    };
}
const newUser={
    firstName:createNewUser("Ali",'GreenHeart').name,
    lastName:createNewUser("Ali",'GreenHeart').surname,
    getLogin: function () {
        return  (this.firstName[0] + this.lastName).toLowerCase();
    },
    setFirstName:function (name) {
        return this.firstName=name;
    },
    setLastName:function (surname) {
    return this.lastName=surname;
    }
};
console.log(newUser.getLogin());
newUser.setFirstName('reza');
// newUser.firstName='zamen';
console.log(newUser.getLogin());
newUser.setLastName('isiyev');
console.log(newUser.getLogin());
alert('I can not use Object.defineProperty() method. I will look . So, we can change the firstName and lastName of the object from outside.');