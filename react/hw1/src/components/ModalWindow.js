import React from "react";
import closeIcon from '../iconClose.png'
import PropTypes from 'prop-types';

class Modal extends React.Component {
    actions = this.props.actionsArr.map((item,i) => <button className={`${this.props.header} decision-btn`} key={i}>{item}</button>);

    render() {
        return (
            <div style={this.props.style} className={`${this.props.header} modal-window`}>
                <h1 className={`${this.props.header} modal-header`}> Do you want to delete {this.props.header} file? </h1>
                <div className="main-part">
                    <p className='modal-text'> {this.props.mainText}</p>
                    <div className='decision-btn-part'> {this.actions} </div>
                    <img src={closeIcon}
                         height='20'
                         width='20'
                         alt='X pic'
                         className='closeBtn'
                         onClick={this.props.clickHandler}
                         data-designation={this.props.header}
                         style={{display: this.props.closeButton ? 'flex' : 'none'}}/>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    mainText: PropTypes.string.isRequired,
    actionsArr: PropTypes.array.isRequired,
    clickHandler: PropTypes.func.isRequired,
    closeButton: PropTypes.bool.isRequired,
    style: PropTypes.object.isRequired,
};
export default Modal