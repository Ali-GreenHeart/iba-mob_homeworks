import React from 'react';
import '../App.scss';
import Button from './Buton'
import Modal from './ModalWindow'


class App extends React.Component {

    state = {
        'first': true,
        'second': true,
        backClrActive: 'lightblue',
        backClrPassive: 'lightgreen'
    };

    openModal = (event) => {
        let index = event.target.dataset.designation;
        this.setState({[index]: false,
        })
    };

    closeModal = (event) => {
        let index = event.target.dataset.designation;
        this.setState({
            [index]: true,
        })
    };

    bcgClick = (event) => {
        if (event.target.classList.contains('modalApp')) {
            this.setState({
                'first': true,
                'second': true,
            })
        }
    };

    render() {
        return (
            <div className="modalApp"
                 style={{background: (!(this.state.first) || !(this.state.second)) ? this.state.backClrActive : this.state.backClrPassive}}
                 onClick={this.bcgClick}>

                <Button clickHandler={this.openModal}
                        numOrder='first'
                        backgroundColor='#d44637'/>
                <Button clickHandler={this.openModal}
                        numOrder='second'
                        backgroundColor='#3c3d89'/>
                <Modal header='first'
                       style={{display: this.state.first ? 'none' : 'flex'}}
                       clickHandler={this.closeModal}
                       closeButton={true}
                       mainText='Once you delete this file, it won’t be possible to undo this action.'
                       actionsArr={['Ok', 'Cancel']}/>
                <Modal header='second'
                       style={{display: this.state.second ? 'none' : 'flex'}}
                       clickHandler={this.closeModal}
                       closeButton={true}
                       mainText='Once you delete this file, it won’t be possible to undo this action.'
                       actionsArr={['Okay', 'Reject']}/>
            </div>
        );
    }
}
export default App;
