import React from 'react';
import './ProductList.scss';
import Product from "./Product/Product";
import Modal from '../Modal/Modal';
import '../Modal/Modal.scss';
const ProductList = (props)=>{
    const addFavHandler = (elNumber)=>{
        let fav = JSON.parse(localStorage.getItem("favorites"));
        if (fav.find(obj => obj.number === elNumber)){
            fav= fav.filter(obj => obj.number !== elNumber);
        } else {
            const result = props.data.find(obj => obj.number === elNumber);
            fav.push(result);
        }
        localStorage.setItem("favorites",JSON.stringify(fav));
        console.log(fav);
    }
    const addCardHandler = (data)=>{
        const cart = JSON.parse(localStorage.getItem("carts"));
        cart.push(data);
        localStorage.setItem("carts",JSON.stringify(cart));
        console.log(cart);
    }
    const modalRef=React.createRef();
    const products = props.data.map(obj=> <Product clickHandler={modalRef} addFavHandler={addFavHandler} self={obj} key={obj.number} /> );
    return(
        <div className={"products"}>
            {products}
            <Modal head={"Adding product to cart"} ref={modalRef} okCondition={addCardHandler} text={"Are you sure you want to add it"}/>
        </div>
    );
}

export default ProductList;