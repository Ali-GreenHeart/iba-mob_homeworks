let ar3=["ali",12,'ahmad',true,'false',3.23,'sain',`asasd`,123,324234,12.43,false];


    //1st solution:  if,else
// function filterBy(arr,itemDatType) {
//     let ar2=[];
//     arr.forEach(item=> {if (typeof (item) !==itemDatType)  ar2.push(item);})
//         return ar2;
// };

    //The Best solution. Instead of if/else i used filter() method of array
const filterBy=(arr,itemDatType)=>arr.filter(it=>typeof(it)!==itemDatType);

console.log(filterBy(ar3, 'number'));
