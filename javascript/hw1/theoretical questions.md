## Theoretical question

1. Explain in your own words the difference between declaring variables via `var`, `let` and `const`. 
2. Why is declaration of a variable via `var` considered a bad tone?


##Answers

1. var is  a bit deprecated, but `let` is not. And, we can change the value of `let` and `var`, but we can't change declared `const` variable.
2. Because, `var` is belong to global or local scope. But `let` is belong to blocked scope. And, we can declare same variable in `var` many times, but in `let` we can't. It'll give an error in `let`. 
