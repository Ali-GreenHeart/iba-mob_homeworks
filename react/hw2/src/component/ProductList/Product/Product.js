import React from 'react';
import './Product.scss';
import Star from './Star'

const Product = ({self,addFavHandler,clickHandler})=>{
    const url = require(`../../../../public/img/${self.path}`);
    const favIcon = React.createRef();
    const AddToFavHandler = () =>{
        favIcon.current.classList.toggle('active');
        addFavHandler(self.number);
    };
    const AddToCartHandler = (e) =>{
        clickHandler.current.openModal(self);
    };
    return(
        <div className="product-card">
            <div className="product-img-wrapper">
                    <img src={url} alt="Product"/>
                    <div className="img-heart">
                        <i ref={favIcon} onClick={AddToFavHandler} title="add to favorite" className="fas fa-heart"/>
                    </div>
            </div>
            <div className="product-info-wrapper">
                <h5 className="head-text">{self.name}</h5>
                <div className="stars">
                    <Star count={1}/>
                    <Star count={2}/>
                    <Star count={3}/>
                    <Star count={4}/>
                    <Star count={5}/>
                </div>
                <p className="info-text">{self.info}</p>
                <div className="footer-info">
                    <button onClick={AddToCartHandler} className="btn-add">Add To Cart</button>
                    <span className="price">${self.price}</span>
                </div>
            </div>
        </div>
    );
};

export default Product;