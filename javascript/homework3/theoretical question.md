object method is ordinary function. But, if you want to invoke it, you have to make this call with the help of `.` (dot) operator. In other word, by using the object name. 

let obj={
let name=promp();
nameToUpper:function(){
return this.name.toUpperCase();
};

if you want to use it, you must use like this:

obj.nameToUpper();